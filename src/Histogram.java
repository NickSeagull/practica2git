public class Histogram<T> {
    // Este comentario es una modificacion del fichero
    HashMap<T, Integer> histogram = new HashMap<>();

    public Histogram(HashMap<T, Integer> histogram){
        this.histogram = histogram;
    }

    public HashMap<T, Integer> getHistogram(){
        return histogram;
    }

    public void setHistogram(HashMap<Integer, T> histogram){
        this.histogram = histogram;
    }
}
