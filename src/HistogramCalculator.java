public class HistogramCalculator<T> {
    Histogram<T> histogram;

    public HistogramCalculator(Histogram<T> histogram){
        this.histogram = histogram;
    }

    public void calculate(ArrayList<T> vector){
        for (int key : vector) {
            if (!histogram.containsKey(key)) {
                histogram.put(key, 1);
            } else {
                histogram.put(key, histogram.get(key) + 1);
            }
        }
    }
}
